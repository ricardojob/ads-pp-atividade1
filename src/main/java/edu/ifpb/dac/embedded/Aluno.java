/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.embedded;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

/**
 *
 * @author Luciana
 */
@Entity
public class Aluno implements Serializable {

    private AlunoPK pk;

    @EmbeddedId
    public AlunoPK getPK() {
        return pk;
    }

}
