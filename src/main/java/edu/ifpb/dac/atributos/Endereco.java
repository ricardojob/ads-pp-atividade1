/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ifpb.dac.atributos;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author Luciana
 */
@Embeddable
public class Endereco implements Serializable{
 
  
    private String nomeRua;
    private String nomeBairro;
    private String nomeCidade;
    private int numero;

    public Endereco() {
    }

    public Endereco(String nomeRua, String nomeBairro, String nomeCidade, int numero) {
        this.nomeRua = nomeRua;
        this.nomeBairro = nomeBairro;
        this.nomeCidade = nomeCidade;
        this.numero = numero;
    }


    public String getNomeRua() {
        return nomeRua;
    }

    public void setNomeRua(String nomeRua) {
        this.nomeRua = nomeRua;
    }

    public String getNomeBairro() {
        return nomeBairro;
    }

    public void setNomeBairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Endereco{" + ""
                + " nomeRua=" + nomeRua + ", nomeBairro=" + nomeBairro + ", nomeCidade=" + nomeCidade + ", numero=" + numero + '}';
    }
}
