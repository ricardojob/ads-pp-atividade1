package edu.ifpb.dac;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author Fatinha de Sousa
 */
public class ImpDao implements Dao{

    private EntityManager entity;
    
    @Override
    public void save(Object o) {
        
        entity = Persistence.createEntityManagerFactory("mapeamento").createEntityManager();
        
        entity.getTransaction().begin();
        entity.persist(o);
        entity.getTransaction().commit();
        
        entity.close();
        
    }

    @Override
    public Object find(Class classe, Object object) {
        
        entity = Persistence.createEntityManagerFactory("mapeamento").createEntityManager();
        
        entity.getTransaction().begin();
        
        Object obj = entity.find(classe, object);
        
        entity.getTransaction().commit();
        
        return obj;
        
    }

    @Override
    public void update(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
