package edu.ifpb.dac;

/**
 *
 * @author Ricardo Job
 */
public interface Dao {

    public void save(Object o);

    public Object find(Class classe, Object object);

    public void update(Object o);

    public void delete(Object o);
}
